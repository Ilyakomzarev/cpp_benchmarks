#pragma once
#include <random>
#include <array>
#include <memory>
#include <algorithm>
#include <time.h>

namespace rl
{
	template<typename Numeric, typename Generator = std::mt19937>
	Numeric random(Numeric from, Numeric to)
	{
		thread_local static Generator gen(time(0));

		using dist_type = typename std::conditional
			<
			std::is_integral<Numeric>::value
			, std::uniform_int_distribution<int64_t>
			, std::uniform_real_distribution<Numeric>
			>::type;

		thread_local static dist_type dist;

		return dist(gen, typename dist_type::param_type{ from, to });
	}

	template<typename Numeric, typename Generator = std::mt19937>
	Numeric random()
	{
		return random<Numeric, Generator>(std::numeric_limits<Numeric>::lowest(), std::numeric_limits<Numeric>::max());
	}

	template<typename Numeric, int COUNT, typename Generator = std::mt19937>
	std::array<Numeric, COUNT> random_array()
	{
		std::array<Numeric, COUNT> ret;

		std::generate(std::begin(ret), std::end(ret),
			static_cast<Numeric(*)()>(random<Numeric, Generator>));

		return ret;
	}

	template<typename Numeric, int COUNT, typename Generator = std::mt19937>
	std::array<Numeric, COUNT> random_array(Numeric from, Numeric to)
	{
		std::array<Numeric, COUNT> ret;

		std::generate(std::begin(ret), std::end(ret), [from,to] {
			return random<Numeric, Generator>(from, to);
		});

		return ret;
	}

	template<typename Container, typename Generator = std::mt19937>
	Container random_container(int count, typename Container::value_type from, typename Container::value_type to)
	{
		Container ret;
		ret.resize(count);
		std::generate(std::begin(ret), std::end(ret),[from, to] {
			return random<Container::value_type, Generator>(from, to);
		});
		return ret;
	}

	template<typename T>
	T choice(std::initializer_list<T> list)
	{
		std::vector<T> v = list;
		int index = random(0, v.size() - 1);
		return v[index];
	}

	inline char choice(const char* str)
	{
		int index = random(0, static_cast<int>(strlen(str) - 1));
		return str[index];
	}

	inline std::unique_ptr<char[]> random_string(int length)
	{
		static const char CODES[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		static const auto SIZE = strlen(CODES) - 1;

		auto ret = std::unique_ptr<char[]>(new char[length + 1]);
		for (int i = 0; i < length; ++i) {
			int index = random(0, static_cast<int>(SIZE) - 1);
			ret[i] = CODES[index];
		}
		ret[length] = '\0';
		return ret;
	}
}
