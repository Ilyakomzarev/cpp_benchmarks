#include "benchmark/benchmark.h"
#include <array>
/*
------------------------------------------------------------
Benchmark                  Time             CPU   Iterations
------------------------------------------------------------
BM_constexpr/50          172 ns          171 ns      4480000
BM_constexpr/100         174 ns          173 ns      4072727
BM_constexpr/500         171 ns          169 ns      4072727
BM_constexpr/1228        170 ns          171 ns      4480000
BM_dynamic/50          14846 ns        15067 ns        49778
BM_dynamic/100         14854 ns        14753 ns        49778
BM_dynamic/500         14678 ns        14439 ns        49778
BM_dynamic/1228        14506 ns        14300 ns        44800
*/

template <int N>
constexpr std::array<int, N> calc_primes()
{
    static_assert(N <= 1228, "");
    const uint32_t NN = 10000;

    std::array<char, NN> vbool{};
    uint32_t count = 0;
    vbool[0] = 1;
    vbool[1] = 1;
    for (uint32_t i = 1; i < NN / 2; i += 2) {
        if (i != 1) {
            for (uint32_t j = i; j < NN / 2; j += 2) {
                auto m = i * j;
                if (m < NN) {
                    vbool[m] = 1;
                } else {
                    break;
                }
            }
        }
    }

    std::array<int, N> ret{};
    for (uint32_t i = 1; i < NN; i += 2) {
        if (vbool[i] != 1) {
            if (count < N) {
                ret[count] = i;
                count++;
            } else {
                return ret;
            }
        }
    }

    return ret;
}

std::vector<int> calc_primes2(int size)
{
    assert(size <= 1228);
    const uint32_t NN = 10000;

    std::array<char, NN> vbool{};
    uint32_t count = 0;
    vbool[0] = 1;
    vbool[1] = 1;
    for (uint32_t i = 1; i < NN / 2; i += 2) {
        if (i != 1) {
            for (uint32_t j = i; j < NN / 2; j += 2) {
                auto m = i * j;
                if (m < NN) {
                    vbool[m] = 1;
                } else {
                    break;
                }
            }
        }
    }

    std::vector<int> ret{};
    ret.resize(size);
    for (uint32_t i = 1; i < NN; i += 2) {
        if (vbool[i] != 1) {
            if (count < size) {
                ret[count] = i;
                count++;
            } else {
                return ret;
            }
        }
    }

    return ret;
}
std::vector<int> constexpr_primes(int count)
{
    constexpr auto primes = calc_primes<1228>();
    std::vector<int> ret;
    ret.assign(primes.begin(), primes.end());
    return ret;
}

std::vector<int> dynamic_primes(int count)
{
    auto primes = calc_primes<1228>();
    std::vector<int> ret;
    ret.assign(primes.begin(), primes.end());
    return ret;
}
static void BM_constexpr(benchmark::State& state)
{
    std::vector<int> primes;
    int count = state.range(0);
    for (auto _ : state) {
        primes = constexpr_primes(count);
    }
    benchmark::DoNotOptimize(primes);
}

static void BM_dynamic(benchmark::State& state)
{
    std::vector<int> primes;
    int count = state.range(0);
    for (auto _ : state) {
        primes = calc_primes2(count);
    }
    benchmark::DoNotOptimize(primes);
}

BENCHMARK(BM_constexpr)->Arg(50)->Arg(100)->Arg(500)->Arg(1228);
BENCHMARK(BM_dynamic)->Arg(50)->Arg(100)->Arg(500)->Arg(1228);
BENCHMARK_MAIN();