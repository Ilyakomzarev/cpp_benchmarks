#include "benchmark/benchmark.h"
#include "flat_map.hpp"
#include "randomtype.hpp"
#include <map>
#include <unordered_map>
/*
----------------------------------------------------------------------
Benchmark                            Time             CPU   Iterations
----------------------------------------------------------------------
BM_InsertString_Map/50            1664 ns         1688 ns       407273
BM_InsertString_Map/1000         98267 ns        98349 ns         7467
BM_InsertString_Map/100000    28224940 ns     28125000 ns           25
BM_InsertString_Flat/50           1886 ns         1859 ns       344615
BM_InsertString_Flat/1000       111190 ns       112305 ns         6400
BM_InsertString_Flat/100000 6305645900 ns   6312500000 ns            1
BM_InsertString_Hash/50            421 ns          430 ns      1600000
BM_InsertString_Hash/1000        13917 ns        13811 ns        49778
BM_InsertString_Hash/100000    3320210 ns      3348214 ns          224
BM_InsertInt_Map/50                323 ns          322 ns      2133333
BM_InsertInt_Map/1000            42949 ns        43316 ns        16593
BM_InsertInt_Map/100000       16471391 ns     16666667 ns           45
BM_InsertInt_Flat/50               293 ns          292 ns      2357895
BM_InsertInt_Flat/1000           35744 ns        35296 ns        19478
BM_InsertInt_Flat/100000    1314065200 ns   1312500000 ns            1
BM_InsertInt_Hash/50              97.4 ns         97.7 ns      6400000
BM_InsertInt_Hash/1000            2432 ns         2400 ns       280000
BM_InsertInt_Hash/100000       1037411 ns      1025391 ns          640
BM_SearchString_Map/50            1185 ns         1193 ns       497778
BM_SearchString_Map/1000         90890 ns        90681 ns         8960
BM_SearchString_Map/100000    27420892 ns     27500000 ns           25
BM_SearchString_Flat/50           1461 ns         1475 ns       497778
BM_SearchString_Flat/1000       105632 ns       104627 ns         7467
BM_SearchString_Flat/100000   20854250 ns     20507813 ns           32
BM_SearchString_Hash/50            442 ns          435 ns      1544828
BM_SearchString_Hash/1000        16644 ns        16741 ns        44800
BM_SearchString_Hash/100000    3130875 ns      3138951 ns          224
*/
auto get_keys = [](int size) {
    std::vector<std::string> ret;
    for (int i = 0; i < size; ++i) {
        auto s = rl::random_string(rl::random(4, 8));
        ret.emplace_back(s.release());
    }
    return ret;
};

//--insert string

template <class T>
static void test_insertString(benchmark::State& state)
{
    T map;
    auto keys = get_keys(state.range(0));
    for (auto _ : state) {
        for (const auto& i : keys) {
            map.emplace(i, 42);
        }
    }

    benchmark::DoNotOptimize(map);
}

BENCHMARK_TEMPLATE(test_insertString, std::map<std::string, int>)
    ->Arg(10)
    ->Arg(1000)
    ->Arg(4000)
    ->Arg(10'000)
    ->Arg(100'000)
    ->Name("BM_InsertString_Map");

BENCHMARK_TEMPLATE(test_insertString, itlib::flat_map<std::string, int>)
    ->Arg(10)
    ->Arg(1000)
    ->Arg(4000)
    ->Arg(10'000)
    ->Name("BM_InsertString_Flat");

BENCHMARK_TEMPLATE(test_insertString, std::unordered_map<std::string, int>)
    ->Arg(10)
    ->Arg(1000)
    ->Arg(4000)
    ->Arg(10'000)
    ->Arg(100'000)
    ->Name("BM_InsertString_Hash");

//-----insert int

template <class T>
void test_insertInt(benchmark::State& state)
{
    T map;
    auto randomInts = rl::random_container<std::vector<int>>((int)state.range(0), 0, 200'000'000);
    for (auto _ : state) {
        for (const auto& i : randomInts) {
            map.emplace(i, 42);
        }
    }

    benchmark::DoNotOptimize(map);
}
BENCHMARK_TEMPLATE(test_insertInt, std::map<int, int>)->Arg(50)->Arg(1000)->Arg(10'000)->Name("BM_InsertInt_Map");

BENCHMARK_TEMPLATE(test_insertInt, itlib::flat_map<int, int>)
    ->Arg(50)
    ->Arg(1000)
    ->Arg(4000)
    ->Arg(10'000)
    ->Name("BM_InsertInt_Flat");

BENCHMARK_TEMPLATE(test_insertInt, std::unordered_map<int, int>)
    ->Arg(50)
    ->Arg(1000)
    ->Arg(4000)
    ->Arg(10'000)
    ->Name("BM_InsertInt_Hash");

//--search string

template <class T>
void test_searchString(benchmark::State& state)
{
    T map;
    auto keys = get_keys(state.range(0));

    for (auto i : keys) {
        map.insert({ i, 42 });
    }

    for (auto _ : state) {
        for (const auto& i : keys) {
            const auto it = map.find(i);
            if (it == map.cend()) {
                assert(false);
            }
        }
    }
}

BENCHMARK_TEMPLATE(test_searchString, std::map<std::string, int>)
    ->Arg(10)
    ->Arg(1000)
    ->Arg(10'000)
    ->Name("BM_SearchString_Map");

BENCHMARK_TEMPLATE(test_searchString, itlib::flat_map<std::string, int>)
    ->Arg(10)
    ->Arg(1000)
    ->Arg(10'000)
    ->Name("BM_SearchString_Flat");
BENCHMARK_TEMPLATE(test_searchString, std::unordered_map<std::string, int>)
    ->Arg(10)
    ->Arg(1000)
    ->Arg(10'000)
    ->Name("BM_SearchString_Hash");

BENCHMARK_MAIN();