import json
import matplotlib.pyplot as plt
import os
import sys


def plot_benchmark_results(json_file):
    with open(json_file, "r") as f:
        data = json.load(f)

    benchmarks = data.get("benchmarks", [])

    results = {}

    for bench in benchmarks:
        name_parts = bench["name"].split("/")
        if len(name_parts) < 2:
            continue
        gp = name_parts[0].split("_")
        if len(gp) < 3:
            group_key = gp[0]
        else:
            group_key = "_".join(gp[:2])

        try:
            x_value = int(name_parts[-1])
        except ValueError:
            continue
        y_value = bench["real_time"]

        if group_key not in results:
            results[group_key] = {}
        if name_parts[0] not in results[group_key]:
            results[group_key][name_parts[0]] = []
        results[group_key][name_parts[0]].append((x_value, y_value))

    for group, benchmarks in results.items():
        plt.figure()
        for title, points in benchmarks.items():
            points.sort()
            x_values, y_values = zip(*points)
            plt.plot(x_values, y_values, marker="o", linestyle="-", label=title)

        plt.xlabel("Input Size")
        plt.ylabel("Real Time (ns)")
        plt.title(f"Benchmark Results - {group}")
        plt.legend()
        plt.grid(True)
        plt.show()


def main(folder_path):
    for filename in os.listdir(folder_path):
        if filename.endswith(".json"):
            file_path = os.path.join(folder_path, filename)
            print(f"Processing {file_path}...")
            plot_benchmark_results(file_path)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit(1)
    main(sys.argv[1])
