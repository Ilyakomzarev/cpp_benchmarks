#include "benchmark/benchmark.h"
#include "randomtype.hpp"
#include <list>
#include <optional>
#include <vector>

/*
-----------------------------------------------------------
Benchmark                 Time             CPU   Iterations
-----------------------------------------------------------
BM_Execption/0       118790 ns       119978 ns         5600
BM_Execption/1      2324334 ns      2351589 ns          299
BM_Execption/5     11113020 ns     11230469 ns           64
BM_Execption/15    32652419 ns     32738095 ns           21
BM_ErrorCode/0       138273 ns       138108 ns         4978
BM_ErrorCode/5       164959 ns       164958 ns         4073
BM_ErrorCode/50      397608 ns       401088 ns         1792
BM_ErrorCode/100     124584 ns       122768 ns         5600
BM_Optional/0        210157 ns       209961 ns         3200
BM_Optional/5        233966 ns       235395 ns         2987
BM_Optional/50       440543 ns       434871 ns         1545
BM_Optional/100      122215 ns       122768 ns         5600
*/
class ExceptionClass
{
public:
    ExceptionClass(int procen)
        : proc(procen)
    {
    }
    __declspec(noinline) void method(int value);

    __declspec(noinline) int method2(int value) noexcept;
    __declspec(noinline) std::optional<int> method3(int value) noexcept;

protected:
    int proc;
};

void ExceptionClass::method(int value)
{
    if (value < proc) {
        throw 10;
    }
}

int ExceptionClass::method2(int value) noexcept
{
    if (value < proc) {
        return -1;
    }
    return 0;
}

std::optional<int> ExceptionClass::method3(int value) noexcept
{
    if (value < proc) {
        return std::nullopt;
    }
    return 0;
}

static auto values = rl::random_container<std::vector<int>>(100'000, 0, 100);
static void BM_Execption(benchmark::State& state)
{

    ExceptionClass asss(state.range(0));
    int expect = 0;

    for (auto _ : state) {
        for (auto i = 0; i < values.size(); ++i) {
            try {
                asss.method(values[i]);
            }
            catch (...) {
                expect++;
            }
        }
    }
    benchmark::DoNotOptimize(expect);
}

BENCHMARK(BM_Execption)->Arg(0)->Arg(1)->Arg(5)->Arg(15)->Arg(50)->Arg(100);

static void BM_ErrorCode(benchmark::State& state)
{
    ExceptionClass asss(state.range(0));

    int expect = 0;

    for (auto _ : state) {
        for (auto i = 0; i < values.size(); ++i) {
            if (asss.method2(values[i]) != 0) {
                expect++;
            }
        }
    }
    benchmark::DoNotOptimize(expect);
}

BENCHMARK(BM_ErrorCode)->Arg(0)->Arg(1)->Arg(5)->Arg(15)->Arg(50)->Arg(100);

static void BM_Optional(benchmark::State& state)
{
    ExceptionClass asss(state.range(0));
    int expect = 0;

    for (auto _ : state) {
        for (auto i = 0; i < values.size(); ++i) {
            auto value = asss.method3(values[i]);
            if (!value.has_value()) {
                expect++;
            }
        }
    }

    benchmark::DoNotOptimize(expect);
}

BENCHMARK(BM_Optional)->Arg(0)->Arg(1)->Arg(5)->Arg(15)->Arg(50)->Arg(100);

BENCHMARK_MAIN();