#include "benchmark/benchmark.h"
#include "randomtype.hpp"
#include <list>
#include <vector>

/*
---------------------------------------------------------------------
Benchmark                           Time             CPU   Iterations
---------------------------------------------------------------------
BM_Insert_List/100              88386 ns        87193 ns         8960
BM_Insert_List/4000           7846607 ns      7672991 ns          112
BM_Insert_List/10000         30955988 ns     30868902 ns           41
BM_Insert_Vector/100          7599949 ns      7609578 ns          154
BM_Insert_Vector/4000         7507788 ns      7550336 ns          149
BM_Insert_Vector/10000        5151168 ns      5156250 ns          100
BM_Insert_VectorBool/100     68940124 ns     68933824 ns           34
BM_Insert_VectorBool/4000    59758524 ns     60000000 ns           25
BM_Insert_VectorBool/10000   55787700 ns     56066176 ns           17
BM_Search_Vector/10               102 ns          101 ns      8960000
BM_Search_Vector/100              823 ns          816 ns       746667
BM_Search_Vector/10000         111215 ns       112305 ns         6400
BM_Search_Vector/10              13.3 ns         13.5 ns     49777778
BM_Search_Vector/100              129 ns          129 ns      4977778
BM_Search_Vector/10000          57970 ns        57813 ns        10000
BM_PushBack_Vector/10             404 ns          396 ns      1659259
BM_PushBack_Vector/100            870 ns          879 ns       746667
BM_PushBack_Vector/10000        16135 ns        16322 ns        34462
BM_PushBack_List/10              1078 ns         1074 ns       640000
BM_PushBack_List/100             5792 ns         5859 ns       112000
BM_PushBack_List/10000         498486 ns       488281 ns         1120
BM_Sort_Vector/10                65.7 ns         64.5 ns      8960000
BM_Sort_Vector/100                601 ns          609 ns      1000000
BM_Sort_Vector/10000           426424 ns       419922 ns         1600
BM_Sort_List/10                   565 ns          578 ns      1000000
BM_Sort_List/100                 6679 ns         6696 ns       112000
BM_Sort_List/10000            1094265 ns      1074219 ns          640
*/

template <class T>
void test_insert(benchmark::State& state)
{
    const auto COUNT = 1000;

    T values(state.range(0), 0);

    auto inserts = rl::random_container<std::vector<T::value_type>>(COUNT, 0, (int)state.range(0));
    for (auto _ : state) {
        for (auto i : inserts) {
            auto it = values.begin();
            std::advance(it, i);
            values.insert(it, i);
        }
    }
}

BENCHMARK_TEMPLATE(test_insert, std::list<int>)->Arg(100)->Arg(4000)->Arg(10'000)->Name("BM_Insert_List");
BENCHMARK_TEMPLATE(test_insert, std::vector<int>)->Arg(100)->Arg(4000)->Arg(10'000)->Name("BM_Insert_Vector");
BENCHMARK_TEMPLATE(test_insert, std::vector<bool>)->Arg(100)->Arg(4000)->Arg(10'000)->Name("BM_Insert_VectorBool");

template <class T>
void test_search(benchmark::State& state)
{
    auto inserts = rl::random_container<T>((int)state.range(0), 0, std::numeric_limits<int>::max());

    for (auto _ : state) {
        for (auto i : inserts) {
            auto it = std::find(inserts.begin(), inserts.end(), i);
            if (it == inserts.end()) {
                assert(false);
            }
        }
    }
}

BENCHMARK_TEMPLATE(test_search, std::vector<bool>)->Arg(10)->Arg(100)->Arg(4000)->Arg(10'000)->Name("BM_Search_Vector");
BENCHMARK_TEMPLATE(test_search, std::list<bool>)->Arg(10)->Arg(100)->Arg(4000)->Arg(10'000)->Name("BM_Search_List");

template <class T>
void test_push_back(benchmark::State& state)
{
    auto inserts = rl::random_container<std::vector<int>>((int)state.range(0), 0, std::numeric_limits<int>::max());

    for (auto _ : state) {
        T cont;

        for (auto i : inserts) {
            cont.push_back(i);
        }

        benchmark::DoNotOptimize(cont);
    }
}

BENCHMARK_TEMPLATE(test_push_back, std::vector<int>)
    ->Arg(10)
    ->Arg(100)
    ->Arg(4000)
    ->Arg(10'000)
    ->Name("BM_PushBack_Vector");
BENCHMARK_TEMPLATE(test_push_back, std::list<int>)->Arg(10)->Arg(100)->Arg(4000)->Arg(10'000)->Name("BM_PushBack_List");

template <class T>
void test_sort(benchmark::State& state)
{
    auto inserts = rl::random_container<T>((int)state.range(0), 0, std::numeric_limits<int>::max());

    for (auto _ : state) {
        auto tmp = inserts;
        std::sort(tmp.begin(), tmp.end());

        benchmark::DoNotOptimize(tmp);
    }
}
BENCHMARK_TEMPLATE(test_sort, std::vector<int>)->Arg(10)->Arg(100)->Arg(4000)->Arg(10'000)->Name("BM_Sort_Vector");

static void BM_Sort_List(benchmark::State& state)
{
    auto inserts = rl::random_container<std::list<int>>((int)state.range(0), 0, std::numeric_limits<int>::max());

    for (auto _ : state) {
        auto tmp = inserts;
        tmp.sort();
        benchmark::DoNotOptimize(tmp);
    }
}

BENCHMARK(BM_Sort_List)->Arg(10)->Arg(100)->Arg(4000)->Arg(10'000);

BENCHMARK_MAIN();